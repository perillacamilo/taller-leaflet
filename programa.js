class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[4.6268343,-74.1165419];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:23
        };


        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion)
    {
        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }

    colocarPoligono (posicion)
    {
        this.poligonos.push(L.polygon(posicion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }


}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.6268343,-74.1165419]);
miMapa.colocarMarcador([4.6268343,-74.1165419]);



miMapa.colocarCirculo([4.6268343,-74.1165419], {
    color: 'red',
    fillColor: 'black',
    fillOpacity: 0.5,
    radius: 500
});



miMapa.colocarPoligono([
    [
        4.6257468654210445,
        -74.1175889968872
        
      ],
      [
        4.624185555841727,
        -74.11565780639648
        
      ],
      [
          4.627479273391039,
        -74.11284685134888
        
      ],
      [ 
          4.62799257864207,
        -74.11561489105225
        
      ],
      [ 4.6257468654210445,
        -74.1175889968872
        
      ]
])
